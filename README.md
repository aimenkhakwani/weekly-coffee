# _Coffee Tracker_

### _A jQuery app that tracks weekly coffee consumption_

#### _**By Ewa Manek and Aimen Khakwani**_

## Description

_This simple app will allow users to add a coffee stamp each time they drink a cup, and tally the total. Not currently persistent_

##Setup and Installation

* _Clone from GitHub_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ewa Manek_**
