$(document).ready(function(){
  var numCups = 0;

  $(".btn-primary").click(function(){
    $(".intro").after('<img src="img/stamp.png" alt="coffee stamp">');
    numCups++;
    $("#numCups").text(numCups);
    $(".intro").next().click(function(){
      $(this).remove();
      numCups--;
      $("#numCups").text(numCups);
    });
  });
  $(".btn-danger").click(function(){
    $("img").remove();
    numCups = 0;
    $("#numCups").text(numCups);
  });

});
